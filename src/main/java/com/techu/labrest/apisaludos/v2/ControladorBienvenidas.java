package com.techu.labrest.apisaludos.v2;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("/api/v1/bienvenidas")
public class ControladorBienvenidas {

    //POJO: Plain Old Java Object
    static class Bienvenida{
        public long id;

        //Atributos públicos Spring los serializa / deserializa
        public String bienvenidaCorta;
        public String bienvenidaLarga;
    }

    private final AtomicLong secuenciaIds = new AtomicLong(0L);
    private final List<Bienvenida> bienvenidas = new LinkedList<Bienvenida>();

    @GetMapping("/")
    public List<Bienvenida> devolverBienvenidas() {
        return this.bienvenidas;
    }

    @PostMapping("/")
    public Bienvenida agregarBienvenida(@RequestBody Bienvenida bienvenida){
        bienvenida.id = (int) this.secuenciaIds.incrementAndGet();
        this.bienvenidas.add(bienvenida);
        return bienvenida;
    }

    @GetMapping("/{id}")
    public Bienvenida soloUnaBienvenida(@PathVariable long id) {
        for(Bienvenida bienvenida : this.bienvenidas){
            if (bienvenida.id == id){
                return bienvenida;
            }
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{id}")
    public void reemplazarBienvenida(@PathVariable long id, @RequestBody Bienvenida bienvenidaNuevo){
        for(int i = 0; i < this.bienvenidas.size(); i++){
            Bienvenida bienvenida = this.bienvenidas.get(i);
            if (bienvenida.id == id){
                bienvenida.bienvenidaCorta = bienvenidaNuevo.bienvenidaCorta;
                bienvenida.bienvenidaLarga = bienvenidaNuevo.bienvenidaLarga;
                return;
            }
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    static class ParcheBienvenida{
        public String operacion;
        public String atributo;
        public Bienvenida bienvenida;
    }

    @PatchMapping("/{id}")
    public void modifBienvenida(@PathVariable long id, @RequestBody ParcheBienvenida bienvenidaModif){
        for(int i = 0; i < this.bienvenidas.size(); i++){
            Bienvenida bienvenida = this.bienvenidas.get(i);
            if (bienvenida.id == id){
                this.bienvenidas.set(i, emparcharBienvenida(bienvenida, bienvenidaModif));
                return;
            }
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    private Bienvenida emparcharBienvenida(Bienvenida bienvenida, ParcheBienvenida bienvenidaModif){
        if(bienvenidaModif.operacion.trim().equalsIgnoreCase("borrar-atributo")) {
            if("bienvenidaCorta".equalsIgnoreCase(bienvenidaModif.atributo)) {
                bienvenida.bienvenidaCorta =null;
            }
            if("bienvenidaLarga".equalsIgnoreCase(bienvenidaModif.atributo)) {
                bienvenida.bienvenidaLarga =null;
            }
        }else if(bienvenidaModif.operacion.trim().equalsIgnoreCase("modificar-atributo")) {
            if(bienvenidaModif.bienvenida.bienvenidaCorta != null)
                bienvenida.bienvenidaCorta = bienvenidaModif.bienvenida.bienvenidaCorta;
            if(bienvenidaModif.bienvenida.bienvenidaLarga != null)
                bienvenida.bienvenidaLarga = bienvenidaModif.bienvenida.bienvenidaLarga;
        }
        return bienvenida;

    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarBienvenida(@PathVariable long id){
        for(int i=0; i < this.bienvenidas.size(); ++i ){
            Bienvenida bienvenida = this.bienvenidas.get(i);
            if(id == bienvenida.id) {
                this.bienvenidas.remove(i);
                return;
            }

        }
    }
}
